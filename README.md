# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
((UKAZI GIT))
git clone https://Bbjber@bitbucket.org/Bbjber/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/Bbjber/stroboskop/commits/306b25130f61b5c776722af9aa15344b17ec336e

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/Bbjber/stroboskop/commits/6a0fdc0db9402dd2724786d1c837c3e7ef8597d2

Naloga 6.3.2:
https://bitbucket.org/Bbjber/stroboskop/commits/11b66368c2bfc899ba1c462836eafa7be463d945

Naloga 6.3.3:
https://bitbucket.org/Bbjber/stroboskop/commits/685c15a36fd4e5858cea4793a947589d459e87aa

Naloga 6.3.4:
https://bitbucket.org/Bbjber/stroboskop/commits/f83685bb5d4a07b80360fb7d8b17a0c5f960b160

Naloga 6.3.5:

```
((UKAZI GIT))
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/Bbjber/stroboskop/commits/97949451e7a6fd44ba3eb1e1a2fd04fc4daf0992

Naloga 6.4.2:
https://bitbucket.org/Bbjber/stroboskop/commits/9d62c7cae128b4463e42a0d6b5124d6c91d21670

Naloga 6.4.3:
https://bitbucket.org/Bbjber/stroboskop/commits/37c0e097af0cff2249872047b8130447b008f8c3

Naloga 6.4.4:
https://bitbucket.org/Bbjber/stroboskop/commits/656ebc2ccb7583c59c0d0316058836fb62651b00